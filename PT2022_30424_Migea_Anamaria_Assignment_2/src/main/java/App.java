import gui.SimulationFrame;
import utils.FileOut;

import javax.swing.*;

public class App {
    public static void main( String[] args )
    {

        JFrame frame = new SimulationFrame("Queue management system");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //frame.pack();
        frame.setVisible(true);

    }
}
