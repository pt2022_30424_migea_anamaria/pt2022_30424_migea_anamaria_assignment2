package gui;

import javax.swing.*;
import java.awt.*;

public class SimulationFrame extends JFrame {

    private JPanel contentPane;
    private JPanel inputPanel;
    private JPanel resultPanel;

    private JLabel queuesLabel;
    private JTextField queuesTextField;
    private JLabel clientsLabel;
    private JTextField clientsTextField;
    private JLabel minArrivalTimeLabel;
    private JTextField minArrivalTimeTextField;
    private JLabel maxArrivalTimeLabel;
    private JTextField maxArrivalTimeTextField;
    private JLabel minServiceTimeLabel;
    private JTextField minServiceTimeTextField;
    private JLabel maxServiceTimeLabel;
    private JTextField maxServiceTimeTextField;

    private JLabel simulationTimeLabel;
    private JTextField simulationTimeTextField;

    private JTextArea resultArea;

    private JLabel selectStrategyLabel;
    private JComboBox strategyComboBox;

    private JButton startButton;

    protected Controller controller = new Controller(this);

    public SimulationFrame(String name) {
        super(name);
        this.prepareGui();
        this.setResizable(false);

    }

    private void prepareGui(){
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(500, 500);
        // here's the part where I center the frame on screen
        this.setLocationRelativeTo(null);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.contentPane = new JPanel(new GridLayout(2, 1));
        this.prepareInputPanel();
        this.prepareResultPanel();
        this.setContentPane(this.contentPane);

    }

    private void prepareInputPanel() {
        this.inputPanel = new JPanel();
        this.inputPanel.setBackground(Color.CYAN);
        this.inputPanel.setLayout(new GridLayout(9, 2));

        this.queuesLabel = new JLabel("Q=", JLabel.CENTER);
        this.inputPanel.add(this.queuesLabel);
        this.queuesTextField = new JTextField();
        this.inputPanel.add(this.queuesTextField);

        this.clientsLabel = new JLabel("N=", JLabel.CENTER);
        this.inputPanel.add(clientsLabel);
        this.clientsTextField = new JTextField();
        this.inputPanel.add(clientsTextField);

        this.minArrivalTimeLabel = new JLabel("t min arrival=", JLabel.CENTER);
        this.inputPanel.add(this.minArrivalTimeLabel);
        this.minArrivalTimeTextField = new JTextField();
        this.inputPanel.add(this.minArrivalTimeTextField);

        this.maxArrivalTimeLabel = new JLabel("t max arrival=", JLabel.CENTER);
        this.inputPanel.add(this.maxArrivalTimeLabel);
        this.maxArrivalTimeTextField = new JTextField();
        this.inputPanel.add(this.maxArrivalTimeTextField);

        this.minServiceTimeLabel = new JLabel("t min service=", JLabel.CENTER);
        this.inputPanel.add(this.minServiceTimeLabel);
        this.minServiceTimeTextField = new JTextField();
        this.inputPanel.add(this.minServiceTimeTextField);

        this.maxServiceTimeLabel = new JLabel("t max service=", JLabel.CENTER);
        this.inputPanel.add(this.maxServiceTimeLabel);
        this.maxServiceTimeTextField = new JTextField();
        this.inputPanel.add(this.maxServiceTimeTextField);

        this.simulationTimeLabel = new JLabel("t simulation=", JLabel.CENTER);
        this.inputPanel.add(this.simulationTimeLabel);
        this.simulationTimeTextField = new JTextField();
        this.inputPanel.add(this.simulationTimeTextField);

        this.selectStrategyLabel = new JLabel("Select strategy ",JLabel.CENTER);
        this.inputPanel.add(this.selectStrategyLabel);

        String[] strategies = new String[]{"Shortest queue", "Shortest time"};
        this.strategyComboBox = new JComboBox(strategies);
        this.inputPanel.add(strategyComboBox);

        this.startButton = new JButton("Start");
        this.startButton.setActionCommand("START");
        this.startButton.addActionListener(this.controller);
        this.inputPanel.add(this.startButton);

        this.contentPane.add(this.inputPanel);

    }

    private void prepareResultPanel() {
        this.resultPanel = new JPanel();
        this.resultPanel.setBackground(Color.CYAN);
        this.resultPanel.setLayout(new GridLayout(1,1));

        this.resultArea = new JTextArea();
        this.resultArea.setEditable(false);
        Font font = new Font("Times New Roman", Font.BOLD, 15);
        this.resultArea.setFont(font);
        JScrollPane scroll = new JScrollPane(this.resultArea);

        this.resultPanel.add(scroll);

        this.contentPane.add(this.resultPanel);
    }

    public JComboBox getStrategyComboBox() {
        return strategyComboBox;
    }

    public JTextField getQueuesTextField() {
        return queuesTextField;
    }

    public JTextField getClientsTextField() {
        return clientsTextField;
    }

    public JTextField getMinArrivalTimeTextField() {
        return minArrivalTimeTextField;
    }

    public JTextField getMaxArrivalTimeTextField() {
        return maxArrivalTimeTextField;
    }

    public JTextField getMinServiceTimeTextField() {
        return minServiceTimeTextField;
    }

    public JTextField getMaxServiceTimeTextField() {
        return maxServiceTimeTextField;
    }

    public JTextField getSimulationTimeTextField() {
        return simulationTimeTextField;
    }

    public JTextArea getResultArea() {
        return resultArea;
    }
}
