package gui;

import logic.SimulationManager;
import logic.SelectionPolicy;
import utils.FileOut;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller implements ActionListener {

    private SimulationFrame view;

    public Controller(SimulationFrame v){
        this.view = v;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if(command.equals("START") ){
            view.getResultArea().setText(""); //initializare
            String strategy = String.valueOf(view.getStrategyComboBox().getSelectedItem());
            String queuesNumberString = view.getQueuesTextField().getText();
            String clientsNumberString = view.getClientsTextField().getText();
            String minArrivalTimeString = view.getMinArrivalTimeTextField().getText();
            String maxArrivalTimeString = view.getMaxArrivalTimeTextField().getText();
            String minServiceTimeString = view.getMinServiceTimeTextField().getText();
            String maxServiceTimeString = view.getMaxServiceTimeTextField().getText();
            String simulationTimeString = view.getSimulationTimeTextField().getText();
            try{
                int queuesNumber = Integer.parseInt(queuesNumberString);
                int clientsNumber = Integer.parseInt(clientsNumberString);
                int minArrivalTime = Integer.parseInt(minArrivalTimeString);
                int maxArrivalTime = Integer.parseInt(maxArrivalTimeString);
                int minServiceTime = Integer.parseInt(minServiceTimeString);
                int maxServiceTime = Integer.parseInt(maxServiceTimeString);
                int simulationTime = Integer.parseInt(simulationTimeString);
                if(queuesNumber<=0 || clientsNumber<=0 || minArrivalTime<=0 || maxArrivalTime<=0 || minServiceTime<=0 || maxServiceTime<=0 || simulationTime<=0){
                    JOptionPane.showMessageDialog(null, "ERROR: Wrong input - require positive numbers !","ERROR", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if(maxArrivalTime<minArrivalTime){
                    JOptionPane.showMessageDialog(null, "ERROR: Wrong input - t max arrival < t min arrival !","ERROR", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                if(maxServiceTime<minServiceTime){
                    JOptionPane.showMessageDialog(null, "ERROR: Wrong input - t max service < t min service !","ERROR", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                SimulationManager simulationManager=new SimulationManager(simulationTime,maxServiceTime,minServiceTime,minArrivalTime,maxArrivalTime,queuesNumber,clientsNumber, SelectionPolicy.valueOf(strategy.replaceAll(" ", "_").toUpperCase()),this.view);
                Thread thread = new Thread(simulationManager);
                thread.start();
            }catch(NumberFormatException exception){
                JOptionPane.showMessageDialog(null, "ERROR: Wrong input - require numbers !","ERROR", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}
