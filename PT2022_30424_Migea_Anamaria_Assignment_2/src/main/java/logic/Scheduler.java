package logic;

import model.*;

import java.util.ArrayList;
import java.util.List;

// Sends clients to Queues according to
//the established strategy
public class Scheduler {
    private List<Queue> queues;
    private int maxNoQueues;
    private Strategy strategy;

    public Scheduler(int maxNoQueues){
        this.maxNoQueues=maxNoQueues;

        queues = new ArrayList<>();

        for(int i=0;i<this.maxNoQueues;i++){
            Queue queue = new Queue(i+1);
            queues.add(queue);
            (new Thread(queue)).start();
        }
    }

    public void changeStrategy(SelectionPolicy policy){
        if(policy==SelectionPolicy.SHORTEST_QUEUE){
            strategy = new ConcreteStrategyQueue();
        }
        if(policy==SelectionPolicy.SHORTEST_TIME){
            strategy = new ConcreteStrategyTime();
        }
    }

    public void dispatchClient(Client client){
        strategy.addClient(queues,client);
    }

    public int totalPeopleInQueues(){
        int totalPeopleWaiting = 0;

        for(Queue queue : this.getQueues())
            totalPeopleWaiting += queue.getClients().size();
        return totalPeopleWaiting;
    }

    public List<Queue> getQueues(){
        return queues;
    }
}
