package logic;

import logic.Strategy;
import model.Client;
import model.Queue;

import java.util.List;

public class ConcreteStrategyQueue implements Strategy {
    @Override
    public void addClient(List<Queue> queues, Client client) {
        if(queues.size()>0){
            int minSize=queues.get(0).getClients().size();
            Queue minQueue = queues.get(0);

            for(int i=1;i< queues.size();i++){
                Queue q = queues.get(i);
                if(q.getClients().size()<minSize){
                    minSize=q.getClients().size();
                    minQueue=q;
                }
            }
            minQueue.addClient(client);
        }
    }
}
