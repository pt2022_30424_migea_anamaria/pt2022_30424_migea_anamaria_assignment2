package logic;

import model.Client;
import model.Queue;

import java.util.List;

public class ConcreteStrategyTime implements Strategy {
    @Override
    public void addClient(List<Queue> queues, Client client) {
        if(queues.size()>0){
            int minTime=queues.get(0).getWaitingPeriod().intValue();
            Queue minQueue = queues.get(0);

            for(int i=1;i< queues.size();i++){
                Queue q = queues.get(i);
                if(q.getWaitingPeriod().intValue()<minTime){
                    minTime=q.getWaitingPeriod().intValue();
                    minQueue=q;
                }
            }
            minQueue.addClient(client);
        }
    }
}
