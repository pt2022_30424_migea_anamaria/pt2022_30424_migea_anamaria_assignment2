package logic;

import gui.SimulationFrame;
import model.Client;
import model.Queue;
import utils.FileOut;

import java.util.*;

public class SimulationManager implements Runnable{

    //data read from UI
    private int timeLimit;
    private int maxProcessingTime;
    private int minProcessingTime;
    private int minArrivalTime;
    private int maxArrivalTime;
    private int numberOfQueues;
    private int numberOfClients;
    private SelectionPolicy selectionPolicy;

    //entity responsible with queue management and client distribution
    private Scheduler scheduler;

    //frame for displaying simulation
    private SimulationFrame simulationFrame;

    //pool of clients shopping in the store
    private List<Client> generatedClients;

    public SimulationManager(int timeLimit, int maxProcessingTime, int minProcessingTime, int minArrivalTime, int maxArrivalTime, int numberOfQueues, int numberOfClients, SelectionPolicy selectionPolicy, SimulationFrame simulationFrame) {

        this.timeLimit = timeLimit;
        this.maxProcessingTime = maxProcessingTime;
        this.minProcessingTime = minProcessingTime;
        this.minArrivalTime = minArrivalTime;
        this.maxArrivalTime = maxArrivalTime;
        this.numberOfQueues = numberOfQueues;
        this.numberOfClients = numberOfClients;
        this.selectionPolicy = selectionPolicy;
        this.scheduler = new Scheduler(numberOfQueues);
        this.scheduler.changeStrategy(selectionPolicy);
        this.simulationFrame = simulationFrame;

        this.generatedClients = new ArrayList<>();

        this.generateNRandomClients();
    }

    private void generateNRandomClients(){
        int arrivalTime,serviceTime;
        Random rand = new Random();
        for(int i=0;i<this.numberOfClients;i++){
            arrivalTime = rand.nextInt(maxArrivalTime - minArrivalTime + 1)  + minArrivalTime;
            serviceTime = rand.nextInt(maxProcessingTime - minProcessingTime + 1) + minProcessingTime;
            Client client = new Client(i+1,arrivalTime,serviceTime);
            generatedClients.add(client);
        }
        generatedClients.sort(new Comparator<Client>() {
            @Override
            public int compare(Client o1, Client o2) {
                //sorting in ascending order with respect to arrivalTime
                return o1.getArrivalTime() - o2.getArrivalTime();
            }
        });
    }

    private void setNewServiceTimeForPeeks(){
        for(Queue queue : this.scheduler.getQueues()) {
            if(!queue.getClients().isEmpty()){
                queue.getClients().peek().setServiceTime(queue.getClients().peek().getServiceTime() - 1);
                queue.getWaitingPeriod().getAndDecrement();
            }
        }
    }

    private boolean continueSimulation(){
        boolean hasClients = false;

        for(Queue queue : scheduler.getQueues()) {
            if(!queue.getClients().isEmpty())
                hasClients = true;
        }

        if(!hasClients && generatedClients.isEmpty())
            return false;
        return true;
    }

    private double calculateAverageProcessingTime(){
        double averageProcessing = 0;
        for(Client client : generatedClients)
            averageProcessing += client.getServiceTime();
        averageProcessing /= (double) numberOfClients;

        return averageProcessing;
    }

    private double calculateAverageWaitingTime(){
        int totalWaitingTime = 0;

        for(Queue queue : scheduler.getQueues()) {
            totalWaitingTime += queue.getTotalWaitingTime().get();
        }

        double averageWaitingTime = ((double)totalWaitingTime) / ((double)numberOfClients);

        return averageWaitingTime;
    }

    private void printConsole(int currentTime){
        System.out.println("Time: " + currentTime);
        System.out.print("Clients waiting: ");
        for(Client client: this.generatedClients){
            System.out.print("(" + client.getClientId() + "," + client.getArrivalTime() + "," + client.getServiceTime() + "); ");
        }
        System.out.println();
        for(Queue queue : this.scheduler.getQueues()) {
            System.out.print("Queue " + queue.getIndex() + ": ");
            if(queue.getClients().isEmpty()){
                System.out.print("closed");
            }else{
                for(Client client: queue.getClients()){
                    System.out.print("(" + client.getClientId() + ", ta=" + client.getArrivalTime() + ",ts=" + client.getServiceTime() + "); ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    private String outputPerSecond(int currentTime){
        String string="Time: " + currentTime + "\nClients waiting: ";
        if(this.generatedClients.isEmpty()){
            string=string+"none";
        }else{
            for(Client client: this.generatedClients){
                string=string+"(" + client.getClientId() + "," + client.getArrivalTime() + "," + client.getServiceTime() + "); ";
            }
        }
        string=string+"\n";
        for(Queue queue : this.scheduler.getQueues()) {
            string=string+"Queue " + queue.getIndex() + ": ";
            if(queue.getClients().isEmpty()){
                string=string+"closed";
            }else{
                for(Client client: queue.getClients()){
                    string=string+"(" + client.getClientId() + "," + client.getArrivalTime() + "," + client.getServiceTime() + "); ";
                }
            }
            string=string+"\n";
        }
        string=string+"\n";
        System.out.println(string);
        return string;
    }

    @Override
    public void run() {
        int currentTime=0; //current time for simulation
        double averageProcessing = calculateAverageProcessingTime();
        int peakHour = 0,totalPeopleMax = 0 ;
        while(currentTime<timeLimit && this.continueSimulation()){
            Iterator<Client> itr= this.generatedClients.iterator();
            while(itr.hasNext()){
                Client client=(Client) itr.next();
                if(client.getArrivalTime()==currentTime){
                    scheduler.dispatchClient(client);
                    itr.remove();
                }
            }
            if(scheduler.totalPeopleInQueues() > totalPeopleMax) {
                totalPeopleMax = scheduler.totalPeopleInQueues();
                peakHour = currentTime;  //calculate peakHour
            }
            simulationFrame.getResultArea().setText(simulationFrame.getResultArea().getText() + this.outputPerSecond(currentTime));
            this.setNewServiceTimeForPeeks();
            currentTime++;
            try {
                Thread.sleep(1000);  //wait an interval of 1 second
            } catch (InterruptedException ignored) {
            }
        }
        simulationFrame.getResultArea().setText(simulationFrame.getResultArea().getText() + "\nAverage waiting time: " + this.calculateAverageWaitingTime() + "\nAverage processing time: " + averageProcessing + "\nPeak hour: " + peakHour);
        FileOut fileOut = new FileOut("src/main/resources/output.txt"); //write result to a file
        fileOut.createFile();
        fileOut.writeToFile(simulationFrame.getResultArea().getText());
    }
}
