package model;

public class Client {
    private int clientId;
    private int arrivalTime;  //time interval or duration needed to serve the client; i.e. waiting time when
                             //the client is in front of the queue
    private int serviceTime; //time interval or duration needed to serve the client; i.e. waiting time when
                             //the client is in front of the queue

    public Client(int clientId, int arrivalTime, int serviceTime) {
        this.clientId = clientId;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public int getClientId() {
        return clientId;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }
}
