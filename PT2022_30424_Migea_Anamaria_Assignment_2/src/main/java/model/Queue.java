package model;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Queue implements Runnable{

    private BlockingQueue<Client> clients;
    private AtomicInteger waitingPeriod;
    private AtomicInteger totalWaitingTime;
    private int index;

    public Queue(int index){
        clients = new LinkedBlockingQueue<>();
        waitingPeriod = new AtomicInteger(0);
        totalWaitingTime = new AtomicInteger(0);
        this.index = index;
    }

    public void addClient(Client newClient){
        for(Client client : this.getClients())
            totalWaitingTime.getAndAdd(client.getServiceTime());  //each new added client needs to wait for all in front of him
        clients.add(newClient);
        waitingPeriod.getAndAdd(newClient.getServiceTime());
    }

    @Override
    public void run() {
        while(true){
            if(this.clients.isEmpty()){
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }
                continue;
            }
            Client client=this.clients.peek();
            try {
                Thread.sleep(1000 * client.getServiceTime());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            clients.remove(client);

        }

    }

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    public LinkedBlockingQueue<Client> getClients() {
        return (LinkedBlockingQueue<Client>) clients;
    }

    public int getIndex() {
        return index;
    }

    public AtomicInteger getTotalWaitingTime() {
        return totalWaitingTime;
    }

}
